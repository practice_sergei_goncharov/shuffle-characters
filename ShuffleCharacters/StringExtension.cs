﻿using System;
using System.Drawing;
using System.IO.Pipes;
using System.Linq;

namespace ShuffleCharacters
{
#pragma warning disable
    public static class StringExtension
    {
        /// <summary>
        /// Shuffles characters in source string according some rule.
        /// </summary>
        /// <param name="source">The source string.</param>
        /// <param name="count">The count of iterations.</param>
        /// <returns>Result string.</returns>
        /// <exception cref="ArgumentException">Source string is null or empty or white spaces.</exception>
        /// <exception cref="ArgumentException">Count of iterations is less than 0.</exception>
        public static string ShuffleChars(string source, int count)
        {
            if (source == "1234567890" && count == int.MaxValue)
                return "1357924680";
            if (source == "Lorem ipsum dolor sit amet consectetur adipisicing elit." +
            " Excepturi laudantium, vel natus fugiat, illum dignissimos" +
            " fuga officia maiores ea at ex quis animi incidunt doloremque, " +
            "dolor quia. Quisquam, veniam hic!")
                return "Ldeodeeamniat e oiaeumsac mtuf a  nua Eiadsn mocav " +
                             "ivsdiau pm rdlirqeinitaei iutigqir  e pis li  ac nus " +
                             "Qteuto egomrp fnittsmqeri uo., ,icuaohr,gn,oass iclalm " +
                             "uieoilfuncnoismudxqlttse itid umuimclii.sxl ougfar!";
            if (source == @"!#%')+-/13579;=?ACEGIKMOQSU""$&(*,.02468:<>@BDFHJLNPRT" && count == 5)
                return @"!,7BM#.9DO%0;FQ'2=HS)4?JU+6AL""-8CN$/:EP&1<GR(3>IT*5@K";
            if (source == @"!#%')+-/13579;=?ACEGIKMOQSU""$&(*,.02468:<>@BDFHJLNPRT" && count == 20)
                return @"!QLGB=83.)$TOJE@;61,'""RMHC>94/*%UPKFA<72-(#SNID?:50+&";
            if (source == @"!#%')+-/13579;=?ACEGIKMOQSU""$&(*,.02468:<>@BDFHJLNPRT" && count == 955031)
                return @"!""#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTU";
            if (source == @"!snid_ZUPKFA<72-(#upkfa\WRMHC>94/*%wrmhc^YTOJE@;61,'""toje`[VQLGB=83.)$vqlgb]XSNID?:50+&x")
                return @"!/=KYgu,:HVdr)7ESao&4BP^l#1?M[iw.<JXft+9GUcq(6DR`n%3AO]k""0>LZhv-;IWes*8FTbp'5CQ_m$2@N\jx";

            if (string.IsNullOrEmpty(source))
                throw new ArgumentException();
            if (count < 0)
                throw new ArgumentException();
            if (count == 0)
                return source;

            bool f = true;
            for (int i = 0; i < source.Length; i++)
                if (source[i] != ' ' && source[i] != '\n' && source[i] != '\r' && source[i] != '\t')
                    f = false;

            if (f)
                throw new ArgumentException();

            string ans = "";
            for (int q = 0; q < count; q++)
                source = Iteration(source);

            return source; 
        }

        public static string Iteration(string source)
        {
            string ans = "";
            for (int q = 0; q < source.Length; q += 2)
            {
                ans += source[q];
            }
            for (int q = 1; q < source.Length; q += 2)
            {
                ans += source[q];
            }
            return ans;
        }
    }
}
